const axios = require("axios")
import { SetActual, GetActual } from "./storage.js";
import { changeContainer, changePop } from "./dom.js";
import { encrypt } from "./crypt.js";

let op = 0;
export let IID = 0;
let Dtag=0;

export const getFormStr = (e) => {
    e.preventDefault();//quito los valores por defecto
    const input = new FormData(e.target).entries()
    //Optenemos el objeto con los datos ingresados
    const dataForm = Object.fromEntries(input)
    //Buscamos los datos
    submitData(dataForm);
}

export const reset = () => {
    //Re reinicia el usuario actual
    SetActual(null)
}

export const showWelcome = () => {
    if (op != 1) {
        //Mensaje de bienvenida con el nombre del usuario 
        op = 1;
        var user = GetActual();
        changeContainer(user, op)
    }
}

export async function showItems() {
    // miro el usuario actual 
    var user = GetActual();
    //Verifico si el container ya existe
    if (op != 2) {
        op = 2;
        // muestro la interfaz del menu
        changeContainer(user, op)
    }
    //obtengo los datos del usuario
    try {
        let lst_item = await axios.get(`http://localhost:3000/items/${user._id}`)
        //Verifico si el usuario si tiene objetos en los datos
        if (lst_item.data.length != 0) {
            changeContainer(lst_item.data, 5)
        } else {
            changeContainer(null, 4);
        }

    } catch (error) {
        console.log(error);
    }
}

export async function showTags() {
    // miro el usuario actual 
    var user = GetActual();
    //Verifico si el container ya existe
    if (op != 3) {
        op = 3;
        // muestro la interfaz del menu
        changeContainer(user, op)
    }
    //obtengo los datos del usuario
    try {
        let lst_tags = await axios.get(`http://localhost:3000/tags/${user._id}`)
        //Verifico si el usuario si tiene objetos en los datos
        if (lst_tags.data.length != 0) {
            changeContainer(lst_tags.data, 6)
        } else {
            changeContainer(null, 4);
        }

    } catch (error) {
        console.log(error);
    }
}

export const whoItem = (e) => {
    const item = e.target.getAttribute("datID");
    return item
}

export const ItemToMod = (idItem) => {
    const btEliminar = document.getElementById("btEliminar")
    btEliminar.addEventListener("click", () => {
        if (window.confirm(`¿Desea eliminar el Item ${idItem}?`)) {
            if (idItem) {
                rmvItem(idItem)
            }
        }
    })
    const btEditar = document.getElementById("btEditar")
    btEditar.addEventListener("click", () => {
        showEd();
        IID = idItem;
        changePop(0);

    })
}


export const whoTag = (e) => {
    const tag = e.target;
    return tag
}

export const TagToMod = (Tag) => {
    let idTag = Tag.getAttribute("datID")
    let datos = Tag.innerHTML;
    const btEtag = document.getElementById("btEtag")
    btEtag.addEventListener("click", () => {
        if (idTag) {
            if (window.confirm(`¿Desea eliminar el tag ${idTag}?`)) {
                rmvTagD(idTag, datos)
            }
        }
    })
    const btEdtag = document.getElementById("btEdtag")
    btEdtag.addEventListener("click", () => {
        showEdTag();
        IID = idTag;
        Dtag = datos;
        changePop(1);
    })
}

async function rmvTagD(idTag, datos) {
    let user = GetActual();
    let Titulo = datos.split(": ", 2)
    console.log(idTag, Titulo[1]);
    let rmv = await axios.delete(`http://localhost:3000/tags/?_id=${idTag}&UID=${user._id}&Titulo=${Titulo[1]}`)
    console.log(rmv);
    menssageInApp("Tag eliminado")
}

export async function editTags(e) {
    let user = GetActual();
    let dataForm = getForm(e);
    let datos = Dtag;
    let Titulo = datos.split(": ", 2)
    let editar = await axios.put(`http://localhost:3000/tags/`,
        {
            tagOrg: {
                _id: IID,
                UID: user._id,
                Titulo: Titulo[1],
            },
            tagNew: dataForm.Nombre
        })
    e.target.reset();
    // menssageInApp("Tag editado");
}


export const msClose = () => {
    if (window.confirm(`¿Desea cerrar sesion?`)) {
        window.location.href = "./index.html"
    }
}


async function rmvItem(idItem) {
    let rmv = await axios.delete(`http://localhost:3000/items/?_id=${idItem}`)
    console.log(rmv);
    menssageInApp("Item eliminado")
}

async function submitData(dataForm) {
    //Si el objeto tiene 3 claves se esta registrando
    if ((Object.keys(dataForm).length) == 3) {
        try {
            let reg = await axios.post("http://localhost:3000/user/",
                {
                    Username: dataForm.username,
                    Password: dataForm.password,
                    Nombre: dataForm.regName
                }
            )
            console.log(reg);
            menssage(reg.data);
        } catch (error) {
            menssage(reg)
            console.log(error);
        }
    } else {
        try {
            let log = await axios.get(`http://localhost:3000/user/?Username=${dataForm.username}&Password=${(dataForm.password)}`)
            console.log(log);
            if (log.data == null) {
                menssage("Usuario no encontrado")
                console.log(log);
            } else {
                welcome();
                SetActual(log.data)
            }
        } catch (error) {
            menssage(log.data)
            console.log(error);
        }

    }
}

// MENSAJES
const menssage = (ms) => {
    //mensajes de informacion
    window.alert(ms);
    window.location.href = "./index.html"
}

const menssageInApp = (ms) => {
    //mensajes de informacion
    window.alert(ms);
    window.location.href = "./items.html"
}

const welcome = () => {
    //Se ingresa el usuario a la app
    window.location.href = "./items.html"
}

//Creacion de nuevos objetos en el HTML

export const showCrear = () => {
    const crearItem = document.getElementById("crearItem");
    crearItem.style.left = "0";
    crearItem.style.opacity = "1";
    crearItem.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(0);
}

export const showCTag = () => {
    const crearTag = document.getElementById("crearTag");
    crearTag.style.left = "0";
    crearTag.style.opacity = "1";
    crearTag.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(1);
}

//ITEMS

export async function crearItem(e) {
    let user = GetActual();
    let dataForm = getForm(e);
    let crear = await axios.post(`http://localhost:3000/items/${user._id}`,
        {
            Titulo: dataForm.title,
            Username: dataForm.userN,
            Password: dataForm.pass,
            URL: dataForm.url,
            Comentario: dataForm.comen,
        })
    e.target.reset();
    dontShow(0)
    // changePop(2)
    // showAddTag()
}

export async function editItem(e) {
    let dataForm = getForm(e);
    let editar = await axios.put(`http://localhost:3000/items/${IID}`,
        {
            Titulo: dataForm.Titulo,
            Username: dataForm.Username,
            Password: dataForm.Password,
            URL: dataForm.URL,
            Comentario: dataForm.Comentario,
        })
    e.target.reset();
    dontShow(2)
    // changePop(2)
    showAddTag()
}

export async function crearTagInItem (e){
    let tagname = getForm(e);
    let editar = await axios.put(`http://localhost:3000/items/${IID}`,
        {
            Tags:[tagname.Tag]
        })
    console.log(editar);
    e.target.reset();
    dontShow(4)
}

// TAGS
export async function crearTags(e) {

    let user = GetActual();
    let dataForm = getForm(e);
    let creartag = await axios.post(`http://localhost:3000/tags/${user._id}`,
        {
            Titulo: dataForm.tagName,
        })
    console.log(creartag);

    e.target.reset();
}

export async function searchTagInItem(datos) {
    let lstItems = await axios.get(`http://localhost:3000/tags/?UID=${datos.UID}&Titulo=${datos.Titulo}`)
    return lstItems
}

const getForm = (e) => {
    e.preventDefault();//quito los valores por defecto
    const input = new FormData(e.target).entries()
    //Optenemos el objeto con los datos ingresados
    const dataForm = Object.fromEntries(input)
    //se retorna el objeto
    return dataForm
}

const showEd = () => {
    const EdItem = document.getElementById("EdItem");
    EdItem.style.left = "0";
    EdItem.style.opacity = "1";
    EdItem.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(2);
}

const showEdTag = () => {
    const EdTag = document.getElementById("EdTag");
    EdTag.style.left = "0";
    EdTag.style.opacity = "1";
    EdTag.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(3);
}

const btDshow = (num) => {
    const btAtras = document.getElementsByClassName("btAtras")[num];
    btAtras.addEventListener("click", () => { dontShow(num) })
}

const dontShow = (num) => {
    switch (num) {
        case 0:
            const crearItem = document.getElementById("crearItem");
            crearItem.style.left = "-100%";
            crearItem.style.opacity = "0";
            crearItem.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showItems();
            break;
        case 1:
            const crearTag = document.getElementById("crearTag");
            crearTag.style.left = "-100%";
            crearTag.style.opacity = "0";
            crearTag.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showTags();
            break;
        case 2:
            const EdItem = document.getElementById("EdItem");
            EdItem.style.left = "-100%";
            EdItem.style.opacity = "0";
            EdItem.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showItems();
            break;
        case 3:
            const EdTag = document.getElementById("EdTag");
            EdTag.style.left = "-100%";
            EdTag.style.opacity = "0";
            EdTag.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showTags();
            break;
        case 4:
            const AddTag = document.getElementById("AddTag");
            AddTag.style.left = "-100%";
            AddTag.style.opacity = "0";
            AddTag.style.transition = "opacity 0.3s, left 0.3s 0.3s";
            showItems();
            break;
    }
}

const showAddTag = () => {
    const AddTag = document.getElementById("AddTag");
    AddTag.style.left = "0";
    AddTag.style.opacity = "1";
    AddTag.style.transition = "opacity 0.2s 0.2s, left 0.2s"
    btDshow(4);
}


