import { crearTagInItem, editTags, getFormStr, reset, showWelcome, showItems, msClose, crearItem, editItem, showTags, crearTags} from "../src/events.js";

const URLactual = document.location;

if (((URLactual.pathname).search("index")) != -1 || ((URLactual.pathname).search("create")) != -1 ){
    reset()
    const formLog = document.getElementById("form-log"); 
    //Optenemos los datos del formulario al hacer submit
    formLog.addEventListener("submit",getFormStr);
}else{
    showWelcome();

    const close = document.getElementById("close")
    close.addEventListener("click",msClose);

    const items = document.getElementById("items");
    items.addEventListener("click",showItems);

    const formItem = document.getElementById("formItem");
    formItem.addEventListener("submit",crearItem)

    const formItemEd = document.getElementById("formItemEd");
    formItemEd.addEventListener("submit",editItem)

    const tags = document.getElementById("tags");
    tags.addEventListener("click",showTags);

    const formTag = document.getElementById("formTag");
    formTag.addEventListener("submit",crearTags)

    const formTagEd = document.getElementById("formTagEd");
    formTagEd.addEventListener("submit",editTags)

    const AddTag= document.getElementById("AddTag")
    AddTag.addEventListener("submit",crearTagInItem)

}
