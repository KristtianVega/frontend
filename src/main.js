//librerias
const { app, BrowserWindow, Menu} = require("electron");
const url = require("url");
const path = require("path");

let loginWindow;

app.on("ready", () => {
    loginWindow = new BrowserWindow({
        title: "Titulo de mi aplicacion",
        fullscreen: false,
        frame: true,
        webPreferences:{
            nodeIntegration:true, //permitir usar las funciones de node
            contextIsolation:false,
        },
    });
    // loginWindow.setFullScreen(true)
    loginWindow.maximize();
    loginWindow.loadURL(
        url.format({
            pathname:path.join(__dirname,"views/index.html"),
            protocol:"file",
            slashes:true,
        })
    )
    
    const menu = Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(menu)
})

const templateMenu=[
    {
        label: "File",
        submenu: [
            {
                label: "Salir",
                accelerator: process.platform == 'darwin' ? 'command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ],
    },
]

if (!app.isPackaged && process.env.NODE_ENV !== "production") {
    templateMenu.push({
        label: 'DevTools',
        submenu: [
            {
            label: "Mostrar menu",
            click(item, focusedWindows) {
                focusedWindows.toggleDevTools();
            },
        }, 
        {
            role: "reload",
        },
        ],
    })
    console.log("agregar dev");
} else {
    console.log("no entro");
}