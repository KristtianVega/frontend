export const SetActual = (user) => {
    //Se guarda el Id actual en el LS
    localStorage.setItem("User", JSON.stringify(user));
}

export const GetActual = () => {
    //Se obtiene el Id actual del LS
    var user = JSON.parse(localStorage.getItem("User"));
    return user
}